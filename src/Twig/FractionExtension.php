<?php


namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FractionExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('fraction', [$this, 'convertToFraction']),
        ];
    }

    public function convertToFraction($n)
    {

        if (is_int($n) || $n==0 || $n == 1)
            return $n;
        $minus = '';
        if($n<0) {
            $n *= -1;
            $minus = '-';
        }
        $tolerance = 1.e-6;
        $h1=1; $h2=0;
        $k1=0; $k2=1;
        $b = 1/$n;
        do {
            $b = 1/$b;
            $a = floor($b);
            $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
            $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
            $b = $b-$a;
        } while (abs($n-$h1/$k1) > $n*$tolerance);
        return $minus."$h1/$k1";
    }

}
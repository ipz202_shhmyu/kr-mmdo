<?php

namespace App\Entity;

use App\Repository\TaskSizeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TaskSizeRepository::class)]
class TaskSize
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: false)]
    private ?int $parameters = 2;

    #[ORM\Column(nullable: false)]
    private ?int $constrs = 2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParameters(): ?int
    {
        return $this->parameters;
    }

    public function setParameters(?int $parameters): static
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function getConstrs(): ?int
    {
        return $this->constrs;
    }

    public function setConstrs(?int $constrs): static
    {
        $this->constrs = $constrs;

        return $this;
    }
}

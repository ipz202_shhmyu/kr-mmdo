<?php

namespace App\Entity;

use App\Repository\SimplexRepository;
use Doctrine\ORM\Mapping as ORM;


class Simplex
{

    private array $targetFunc = [];

    private int $BIG_NEGATIVE_NUMBER = -1000000;
    private int $BIG_NUMBER = 1000000;
    private array $limitations = [];


    private array $limitationsCanonical;

    private array $mathSymbols = [['<=', 1], ['>=', -1], ['=', 0]];

    private Table $table;

    private array $basis = [];

    public function __construct(array $targetFunc, array $limitations)
    {
        $this->targetFunc = $targetFunc;
        $this->limitations = $limitations;
    }

    public function run()
    {

        if(end($this->targetFunc) === 'min')
            $this->reverseTargetFunc();


        $this->limitationsCanonical = $this->canonicalForm();

        $test = $this->checkingForTheUnitMatrix();

        if (sizeof($test) > 0)
            $this->addAnArtificialBasis($test);

        $this->createTable();

        //$tableToRender[] = $this->ReadyUpTable($this->table);
        $result = $this->decision();
        $tableToRender = [];
        $tableToRender [] = $this->table->ReadyUpTable($result['delta']);
//        array_push($tableToRender, $this->ReadyUpTable($this->table,$result['delta']));
        //dump($this->ReadyUpTable($this->table,$result['delta']));


        do {
            if ($result['status'] === 'recalculation') {
                $isImpossible = $this->recalculation($result['delta']);

                if(!($isImpossible)) {
                    $tableToRender[] = $this->table->ReadyUpTable($result['delta']);
                    $result['status'] === 'impossible';
                    break;
                }
                $result = $this->decision();
                //dump($this->ReadyUpTable($this->table,$result['delta']));
                //array_push($tableToRender, $this->ReadyUpTable($this->table,$result['delta']));
                $tableToRender [] = $this->table->ReadyUpTable($result['delta']);
                //$tableToRender[] = $this->ReadyUpTable($this->table);

            } else {
                break;
            }

        } while (1);

        $tableToRender[] = $result;
        $tableToRender[] = $this->limitationsCanonical;
       return $tableToRender;
    }




    protected function whatMathSymbol(array $limitation)
    {
        foreach ($limitation as $value) {
            foreach ($this->mathSymbols as $symbolMath) {
                if ($value === $symbolMath[0])
                    return $symbolMath[1];
            }
        }

        return -2;
    }


    protected function symbolIndex(string $symbol)
    {
        foreach ($this->mathSymbols as $symbolMath) {
            if ($symbol === $symbolMath[0])
                return $symbolMath[1];
        }

        return -2;
    }


    protected function canonicalForm()
    {
        $limitationsCanonical = $this->limitations;


        /**
         * Check if B is a negative number then change the signs
         */
        for ($i = 0; $i < sizeof($limitationsCanonical); $i++) {
            if (end($limitationsCanonical[$i]) <= 0) {
                for ($j = 0; $j < sizeof($limitationsCanonical[$i]); $j++) {
                    if (!is_string($limitationsCanonical[$i][$j])) {
                        $limitationsCanonical[$i][$j] *= -1;
                    } else {
                        if ($this->symbolIndex($limitationsCanonical[$i][$j]) === -1)
                            $limitationsCanonical[$i][$j] = '<=';
                        else
                            $limitationsCanonical[$i][$j] = '>=';
                    }
                }
            }
        }

        /**
         * DOC
         */
        for ($i = 0; $i < sizeof($limitationsCanonical); $i++) {
            for ($j = 0; $j < sizeof($limitationsCanonical[$i]); $j++) {

                if ($this->symbolIndex($limitationsCanonical[$i][$j]) === 1 || $this->symbolIndex($limitationsCanonical[$i][$j]) === -1) {

                    if ($this->symbolIndex($limitationsCanonical[$i][$j]) === 1)
                        array_splice($limitationsCanonical[$i], $j, 0, array(1));
                    else
                        array_splice($limitationsCanonical[$i], $j, 0, array(-1));

                    $limitationsCanonical[$i][$j + 1] = '=';

                    array_splice($this->targetFunc, $j, 0, array(0));

                    for ($z = 0; $z < sizeof($limitationsCanonical); $z++) {
                        if ($z != $i)
                            array_splice($limitationsCanonical[$z], $j, 0, array(0));
                    }
                }
            }
        }

        return $limitationsCanonical;
    }


    protected function checkingForTheUnitMatrix()
    {

        $limitationIndex = range(1, (sizeof($this->limitationsCanonical)));

        $unitColumn = false;

        for ($i = 0; $i < sizeof($this->targetFunc) - 1; $i++) {

            $numberOfZeros = 0;
            $possibleLimitation = -1;

            for ($j = 0; $j < sizeof($this->limitationsCanonical); $j++) {

                if ($this->limitationsCanonical[$j][$i] === 1) {
                    $unitColumn = true;

                    $possibleLimitation = $j;

                    continue;
                }

                if ($this->limitationsCanonical[$j][$i] === 0) {
                    $numberOfZeros++;

                    continue;
                } else
                    $unitColumn = false;
            }

            if ($unitColumn === true && $numberOfZeros === sizeof($this->limitationsCanonical) - 1) {
                array_push($this->basis, $i);
                unset($limitationIndex[$possibleLimitation]);
            }
        }

        return $limitationIndex;
    }


    protected function addAnArtificialBasis(array $limitationsIndexes)
    {
        foreach ($limitationsIndexes as $key) {

            for ($j = 0; $j < sizeof($this->limitationsCanonical[$key - 1]); $j++) {

                if ($this->symbolIndex($this->limitationsCanonical[$key - 1][$j]) === 0) {

                    array_splice($this->limitationsCanonical[$key - 1], $j, 0, array(1));
                    array_push($this->basis, $j);

                    if (end($this->targetFunc) === 'max') {
                        array_splice($this->targetFunc, $j, 0, array($this->BIG_NEGATIVE_NUMBER));
                        break;
                    } else {
                        array_splice($this->targetFunc, $j, 0, array($this->BIG_NUMBER));
                        break;
                    }
                }
            }


            for ($i = 0; $i < sizeof($this->limitationsCanonical); $i++) {
                for ($j = 0; $j < sizeof($this->limitationsCanonical[$i]); $j++) {

                    if ($this->symbolIndex($this->limitationsCanonical[$i][$j]) === 0) {

                        if ($i != intval($key - 1)) {
                            array_splice($this->limitationsCanonical[$i], $j, 0, array(0));
                            break;
                        }
                    }
                }
            }
        }

        // debug($this->targetFunc);
    }

    protected function createTable()
    {
        $this->table = new Table($this->targetFunc, $this->limitationsCanonical, $this->basis);
    }

    protected function decision()
    {

        // status: recalculation, decided, impossible
        $result = [
            "status" => ''
        ];

        $delta = [];
        $tmpDelta = 0;

        // Считает дельту
        for ($i = 0; $i < $this->table->getRowsTable(); $i++) {

            for ($j = 0; $j < $this->table->getCallsTable(); $j++) {
                $tmpDelta += $this->table->getTargetFuncVariables()[$this->table->getBasis()[$j]] * $this->table->getTable()[$j][$i];
            }

            $tmpDelta -= $this->table->getTargetFuncVariables()[$i];

            array_push($delta, $tmpDelta);

            $tmpDelta = 0;
        }

        for ($j = 0; $j < $this->table->getCallsTable(); $j++) {
            $tmpDelta += $this->table->getTargetFuncVariables()[$this->table->getBasis()[$j]] * $this->table->getB()[$j];
        }

        array_push($delta, $tmpDelta);
        $tmpDelta = 0;

        // Проверяем если в дельте есть отрицательное число, то ставим статус пересчет
        foreach ($delta as $value) {
            if ($value == end($delta))
                continue;

            if ($value < 0) {
                $result['status'] = 'recalculation';
                $result += ['delta' => $delta];
          //     dump($delta);
                return $result;
            }
        }


        $answer = 0;
        $points = [];
        for ($i = 0; $i < $this->table->getCallsTable(); $i++) {
            $d = $this->table->getTargetFuncVariables()[$this->table->getBasis()[$i]] * $this->table->getB()[$i];
            if($d != 0)
            {
                $answer+= $d;
                $points []=[$this->table->getBasis()[$i], $this->table->getB()[$i]];
            }
        }
        //dump($delta);
        $result['delta'] = $delta;
        $result['status'] = 'decided';
        $result['answer'] = $answer;
        $result['points'] = $points;
        return $result;
    }

    protected function reverseTargetFunc()
    {
        foreach($this->targetFunc as $key => $value)
        {
            if(end($this->targetFunc) === $value)
                $this->targetFunc[$key] = 'max';
            else
                $this->targetFunc[$key] = $value * (-1);
        }
    }


    protected function recalculation(array $delta)
    {

        $newX = 0;
        $replacementX = 0;

        $replacementXMatrix = [];

        $table = $this->table->getTable();

        $B = $this->table->getB();


        // Ищем в дельте самое большое отрицательное число
        $min = 1000000;
        foreach ($delta as $key => $value) {

            if ($value == end($delta)) {
                continue;
            } else {
                if ($value < $min) {
                    $newX = $key;
                    $min = $value;
                }
            }
        }


        // Ищем иксы для замены
        for ($i = 0; $i < $this->table->getCallsTable(); $i++) {

            if ($this->table->getTable()[$i][$newX] <= 0) {
                array_push($replacementXMatrix, '-');
                continue;
            } else
                array_push($replacementXMatrix, $this->table->getB()[$i] / $this->table->getTable()[$i][$newX]);
        }

        $impossible = true;
        // Проверяем мозможно ли пересчитать таблицу
        foreach ($replacementXMatrix as $key => $value) {
            if ($value !== '-')
            {
                $impossible = false;
                break;
            }
        }

        if($impossible)
            return false;

        $min = 1000000;
        foreach ($replacementXMatrix as $key => $value) {
            if ($value === '-')
                continue;

            if ($value < $min) {
                $replacementX = $key;
                $min = $value;
            }
        }

        // Перерасчет таблицы
        $this->basis[$replacementX] = $newX;
        $tmp = $table[$replacementX][$newX];

        for ($i = 0; $i < $this->table->getRowsTable(); $i++) {
            $table[$replacementX][$i] /= $tmp;
        }

        $B[$replacementX] /= $tmp;

        for ($i = 0; $i < $this->table->getCallsTable(); $i++) {
            $tmpRow = [];

            if ($replacementX === $i)
                continue;


            $tmp = $table[$i][$newX];

            for ($j = 0; $j < $this->table->getRowsTable(); $j++) {
                array_push($tmpRow, $table[$replacementX][$j] * - ($tmp));
            }

            for ($j = 0; $j < $this->table->getRowsTable(); $j++) {
                $table[$i][$j] += $tmpRow[$j];
            }

            $B[$i] += - ($tmp) * $B[$replacementX];

            unset($tmpRow);
        }


        $this->table->updateTable($table, $this->basis, $B);

        return true;
    }
}

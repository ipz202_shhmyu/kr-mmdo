<?php

namespace App\Entity;

use App\Repository\TableRepository;
use Doctrine\ORM\Mapping as ORM;


class Table
{
    private int $rowsTable;

    private int $callsTable;

    private array $targetFuncVariables = [];

    private array $table = [];

    private array $basis = [];

    private array $cbasis = [];
    private array $B = [];

    private array $delta = [];

    public function __construct(array $targetFunc, array $limitations, array $basis)
    {
        $this->createTable($targetFunc, $limitations, $basis);
    }

    public function getTable() {
        return $this->table;
    }
    public function setCBasis(array $cb) {
        $this->cbasis = $cb;
    }
    public function getCBasis() {
        return $this->cbasis;
    }
    public function getRowsTable()
    {
        return $this->rowsTable;
    }

    public function getCallsTable(){
        return $this->callsTable;
    }

    public function getB() {
        return $this->B;
    }

    public function getDelta() {
        return $this->delta;
    }
    public function setDelta(array $del) {
        $this->delta = $del;
    }
    public function getTargetFuncVariables() {
        return $this->targetFuncVariables;
    }

    public function getBasis() {
        return $this->basis;
    }

    protected function createTable(array $targetFunc, array $limitations, array $basis)
    {

        $this->rowsTable = sizeof($targetFunc) - 1;

        $this->callsTable = sizeof($limitations);

        for ($i = 0; $i < $this->callsTable; $i++) {
            array_push($this->B, end($limitations[$i]));

            array_push($this->table, array());

            for ($j = 0; $j < $this->rowsTable; $j++) {
                array_push($this->table[$i], $limitations[$i][$j]);
            }
        }

        for ($i = 0; $i < $this->rowsTable; $i++) {
            array_push($this->targetFuncVariables, $targetFunc[$i]);
        }

        $this->basis = $basis;

    }

    public function updateTable(array $table, array $basis, array $B) {
        unset($this->table);
        unset($this->basis);
        unset($this->B);

        $this->table = $table;
        $this->basis = $basis;
        $this->B = $B;
    }
    public function ReadyUpTable(array $delta)
    {
        $cb = [];
        for ($i = 0; $i < $this->getCallsTable(); $i++) {
            $cb[] = $this->getTargetFuncVariables()[$this->getBasis()[$i]];
        }
        $lastElement = array_pop($delta);
        array_unshift($delta, $lastElement);

        $result['table'] = $this->table;
        $result['basis'] = $this->basis;
        $result['rowsTable'] = $this->rowsTable;
        $result['callsTable'] = $this->callsTable;
        $result['targetFuncVariables'] = $this->targetFuncVariables;
        $result['cbasis'] = $cb;
        $result['B'] = $this->B;
        $result['delta'] = $delta;


    return $result;
    }

}

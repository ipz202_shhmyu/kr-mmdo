<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimplexFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parameters = $options['parameters'];
        $constrs = $options['constrs'];
        $func = $builder->create('f', FormType::class, [
            'label' => false,
        ]);

        for ($i = 1; $i <= $parameters; $i++) {
            $func->add('x_' . $i, IntegerType::class, [
                'label' => 'x' . $i,
                'required' => true,
                'attr' => [
                    'class' => 'form-control mr-3',

                ],
                'label_attr' => [
                    'class' => 'mr-2 ml-1',
                ],
            ]);
        }
        $func->add('dir', ChoiceType::class, [
            'label' => '=>',
            'choices' => [
                'max' => 'max',
                'min' => 'min',
            ],
            'attr' => [
                'class' => 'form-control mr-2 ml-2'
            ],
            'required' => true,
        ]);

        $builder->add($func);

        for ($rowIndex = 1; $rowIndex <= $constrs; $rowIndex++)
        {
            $row = $builder->create('row_' . $rowIndex, FormType::class, [
                'label' => false,
            ]);

            for ($j = 1; $j <= $parameters; $j++) {
                $row->add($i . 'x_' . $j, IntegerType::class, [
                    'label' => 'x' . $j,
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control mr-1 ',

                    ],
                    'label_attr' => [
                        'class' => 'mr-2 ml-1',
                    ],
                ]);
            }
            $row->add('con', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    '<=' => '<=',
                    '>=' => '>=',
                    '=' => '=',
                ],
                'attr' => [
                    'class' => 'form-control mr-4 ml-3 ',
                ],
                'required' => true,
            ]);
            $row->add($i . 'x_res', IntegerType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control col-6',
                ],
            ]);


            $builder->add($row);
        }
        $builder->add('submit', SubmitType::class, ['label' => 'Далі',
            'attr' => [
                'class' => 'mt-3 btn btn-dark'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('parameters');
        $resolver->setRequired('constrs');
        $resolver->setAllowedTypes('parameters', 'integer');
        $resolver->setAllowedTypes('constrs', 'integer');
    }

}
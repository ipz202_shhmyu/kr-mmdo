<?php

namespace App\Form;

use App\Entity\TaskSize;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class TaskSizeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('parameters', IntegerType::class, [
                'label' => 'К-ть змінних',
                'attr' => [
                    'min' => 2,
                    'max' => 4,
                    'value' => 2,
                    'class' => 'form-control'
                ]

            ])
            ->add('constrs', IntegerType::class, [
                'label' => 'К-ть обмежень',
                'attr' => [
                    'min' => 2,
                    'max' => 10,
                    'value' => 4,
                    'class' => 'form-control'
                ]

            ])
            ->add('submit', SubmitType::class, ['label' => 'Далі',
                'attr' => [
                'class' => 'mt-3 btn btn-dark'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TaskSize::class,
        ]);
    }
}

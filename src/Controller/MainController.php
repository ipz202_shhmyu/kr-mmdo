<?php

namespace App\Controller;


use App\Entity\Simplex;
use App\Entity\Table;
use App\Form\SimplexFormType;
use App\Form\TaskSizeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private $cache;

    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @Route("/", name="tasksize")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(TaskSizeType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Process the form submission
            $formData = $form->getData();

            // Store the form data in cache
            $cacheItem = $this->cache->getItem('form_data');
            $cacheItem->set($formData);
            $this->cache->save($cacheItem);

            // Redirect to another page or show a success message
            return $this->redirectToRoute('matrixform');
        }

        return $this->render('form/index.html.Twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/matrixform", name="matrixform")
     */

    public function matrixForm(Request $request, CacheItemPoolInterface $cache)
    {
        // Retrieve the form data from cache
        $cacheItem = $cache->getItem('form_data');
        $formData = $cacheItem->get();

        // Access the parameters and constraints property
        $parameters = $formData->getParameters();
        $constrs = $formData->getConstrs();
        // Create the dynamic form
        $form = $this->createForm(SimplexFormType::class, null, [
            'parameters' => $parameters,
            'constrs' => $constrs,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Get the form data as an array
            $data = $form->getData();
            $targetFunc = [];
            foreach ($data['f'] as $value)
                $targetFunc[] = $value;


            $result = [];

            foreach ($data as $key => $value) {
                if (strpos($key, 'row_') === 0) {
                    $result[] = array_values($value);
                }
            }
            $matrix = $result;
            $data = [$targetFunc, $matrix];

            // Store the form data in cache
            $cacheItem = $cache->getItem('simplex_form_data');
            $cacheItem->set($data);
            $cache->save($cacheItem);

            // Redirect to another page or show a success message
            return $this->redirectToRoute('calculation');
        }
        return $this->render('form/simplex_form.html.Twig', [
            'parameters'=>$parameters,
            'constraints'=>$constrs,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/calculation", name="calculation")
     */
    public function Calculation(CacheItemPoolInterface $cache)
    {
        // Retrieve the dynamic form data from cache
        $cacheItem = $cache->getItem('simplex_form_data');
        $formData = $cacheItem->get();
        $targetFunc = $formData[0];
        $matrix = $formData[1];
        $simplex = new Simplex($targetFunc,$matrix);
        $result = $simplex->run();
        $canonical = array_pop($result);
        $answer = array_pop($result);
        return $this->render('form/calculation.html.Twig', [
            'targetFunc' => $targetFunc,
            'matrix' => $matrix,
            'canonical' => $canonical,
            'tables' => $result,
            'answer' => $answer,
        ]);
    }
}
